const expect = require('chai').expect;
const {calc} = require('../src/calc');

describe("Calc", () => {
    it("Should return 'calc' if the sum is 5", () => {
        expect(calc(3, 2)).to.equal(5);
    });
    it("Should return 'calc' if the sum is -1", () => {
        expect(calc(-3, 2)).to.equal(-1);
    });
    it("Should return 'error' if the num is NaN", () => {
        expect(calc('a', 'b')).to.equal('Error');
    });
    it("Should return 'calc' if the sum is 0,8", () => {
        expect(calc((0,1) ,(0,7) )).to.equal(0,8);
    });
});